const btnItens = document.querySelectorAll('.links');

btnItens.forEach(item => {
    item.addEventListener('click', scrollToId);
});
function getScrollToHref(element) {
    const id = element.getAttribute('href');
    return document.querySelector(id).offsetTop;
}
function scrollToId(event) {
    event.preventDefault();
    const section = getScrollToHref(event.target) - 50;
    scrollToPosition(section);
}

function scrollToPosition(section){
    window.scroll({
        top: section,
        behavior: "smooth"
    });
}
$(document).ready(function(){
    $('.telefone').inputmask("(99) 99999-9999");
    // $(".capital").inputmask({
    //     mask: 'R$ [9][9][9][9][9][9][9][9]9,99',
    //     numericInput: true,
    //     placeholder: '0',
    //     greedy: false
    // });
});
//formulario aparecer
const indicaAmigo = document.querySelector("#quero-indicar");
const indica = document.querySelector(".indicar");
const naoIndica = document.querySelector("#nao-indicar");
const amigos = document.querySelector(".indicacao");

indicaAmigo.addEventListener('click', function(){
    indica.style.display = "none";
    naoIndica.style.display = "none";
    amigos.style.display = "block";
});

//preencher tabela de indicar de amigos
$(document).ready(function(){
    $('#formAmigo').submit(function () {
        var $this = $(this);
        var tr = '<tr>'+
            '<td scope="col" class="text-center">'+$this.find("input[name='name']").val()+'</td>'+
            '<td scope="col" class="text-center">'+$this.find("input[name='phone']").val()+'</td>'+
            '<td scope="col" class="text-center">'+$this.find("input[name='email']").val()+'</td>'+
            '</tr>'
        $('#tableAmigos').find('tbody').append(tr);
        $('#formAmigo input').val("");
        return false;
    });
});
$(document).ready(function(){
    $('#formAmigo').each(function(){
        this.reset();
    });
});
//adicionar dados dos amigos
var hiddens = '<input type="hidden" name="name[]" value="'+name+'" />'+
    '<input type="hidden" name="phone[]" value="'+phone+'" />'+
    '<input type="hidden" name="email[]" value="'+email+'" />';

$('#form_insert').find('fieldset').append( hiddens );

const btnQtdAmigos = document.querySelector("#indicaAmigo");
var txtQtdAmigos = document.querySelector(".qtdAmigos");
var qtdAmigos = document.querySelector(".qtdAmigos span");
btnQtdAmigos.addEventListener("click" , function (){
    var tabela = document.getElementById('tableAmigos');
    var linhas = tabela.getElementsByTagName('tr');
    qtdAmigos.innerHTML = linhas.length;
    txtQtdAmigos.style.display = "block";
});